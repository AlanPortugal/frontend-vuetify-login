import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const guardApp = (to, from, next) => {
  const _existeToken = window.localStorage.getItem('token')
  if (_existeToken) {
    next()
  } else {
    next('/')
  }
}

const guardLogin = (to, from, next) => {
  const _existeToken = window.localStorage.getItem('token')
  if (_existeToken) {
    return next('/app')
  } else {
    return next()
  }
}

const routes = [
  {
    path: '/',
    name: 'OutLogin',
    component: () => import('../layouts/OutLoginLayout.vue'),
    beforeEnter: guardLogin,
    children: [
      {
        path: '',
        name: 'Login',
        component: () => import('../views/Login.vue')
      }
    ]
  },
  {
    path: '/adm',
    name: 'Administrador',
    component: () => import('../layouts/AdministradorLayout.vue'),
    beforeEnter: guardApp
  },
  {
    path: '/tec',
    name: 'Tecnico',
    component: () => import('../layouts/TecnicoLayout.vue'),
    beforeEnter: guardApp
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
